Name:            libcdio
Version:         2.2.0
Release:         1
Summary:         CD-ROM input and control library
License:         GPL-3.0-or-later
URL:             https://www.gnu.org/software/libcdio/
Source0:         https://github.com/libcdio/libcdio/releases/download/%{version}/%{name}-%{version}.tar.bz2
Source2:         libcdio_x86_64_config.h

BuildRequires:   gcc gcc-c++ pkgconfig doxygen ncurses-devel help2man chrpath gettext-devel

%description
The GNU Compact Disc Input and Control library (libcdio) contains a library for CD-ROM and CD image access.
Applications wishing to be oblivious of the OS- and device-dependent properties of a CD-ROM or of the specific 
details of various CD-image formats may benefit from using this library.

%package devel
Summary: Header files and development Files for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
This package contains header files and development Files for for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1
iconv -f ISO88591 -t utf-8 -o THANKS.utf8 THANKS && mv THANKS.utf8 THANKS

%build
%configure  --disable-rpath --disable-vcd-info --disable-dependency-tracking --disable-cddb
%make_build

cd doc/doxygen
sed -i -e  "s,EXCLUDE .*$,EXCLUDE = ../../include/cdio/cdio_config.h,g;" Doxyfile

./run_doxygen

%install
%make_install
%delete_la
if [ `uname -i` =  x86_64 ]; then
  mv $RPM_BUILD_ROOT%{_includedir}/cdio/cdio_config.h $RPM_BUILD_ROOT%{_includedir}/cdio/cdio_config_`uname -i`.h
  install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_includedir}/cdio
fi
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

%check
%make_build check

%files
%doc AUTHORS 
%license COPYING
%{_bindir}/*
%{_libdir}/libcdio++.so.*
%{_libdir}/libcdio.so.*
%{_libdir}/libiso9660++.so.*
%{_libdir}/libiso9660.so.*
%{_libdir}/libudf.so.*

%files devel
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libcdio++.so
%{_libdir}/libcdio.so
%{_libdir}/libiso9660++.so
%{_libdir}/libiso9660.so
%{_libdir}/libudf.so
%{_libdir}/*.a
%{_includedir}/cdio
%{_includedir}/cdio++

%files help
%doc NEWS.md README.md README-libcdio.md THANKS TODO
%{_mandir}/man1/*
%{_infodir}/*

%changelog
* Mon Jan 20 2025 Funda Wang <fundawang@yeah.net> - 2.2.0-1
- update to 2.2.0

* Tue Jan 17 2023 zhouwenpei <zhouwenpei@h-partners.com> - 2.1.0-6
- fix test case fail

* Mon Apr 18 2022 Chenxi Mao <chenxi.mao@suse.com> - 2.1.0-5
- Remove self-dependency

* Fri Mar 11 2022 JunYang <jun.yang@suse.com> - 2.1.0-4
- Fix compiler error for option -Werror=format-security

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.1.0-3
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Feb 9 2021 jinzhimin <jinzhimin2@huawei.com> - 2.1.0-2
- add old so file

* Fri Jan 29 2021 zhanzhimin <zhanzhimin@huawei.com> - 2.1.0-1
- update to 2.1.0

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.0-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: optimization the spec

* Mon Jan 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.0-7
- update software package

* Sat Aug 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.0-6
- Type:bugfix
- ID:NA
- SUG:NA

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.0-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: openEuler Debranding

* Wed Aug 14 2019 sunyun <sunyun9@huawei.com> - 2.0.0-4
- Package init

